﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using clsMariaDB;

namespace BotGenerator
{
    public partial class frmBotGenerator : Form
    {
        //string[] tables = "inbox_center_smses inbox_center_ircs inbox_center_sockets".Split(' ');
        string[] tables = "#1bot_msisdn_targets".Split(' ');

        MariaDB[] db;
        Dictionary<string, string>[] dbQueryResults;
        Timer[] tmrTimer;

        public frmBotGenerator()
        {
            InitializeComponent();
        }

        private void frmBotGenerator_Load(object sender, EventArgs e)
        {
            this.Activated += AfterLoading;
        }

        private void AfterLoading(object sender, EventArgs e)
        {
            this.Activated -= AfterLoading;

            tmrTimer = new Timer[tables.Count()];
            db = new MariaDB[tables.Count()];

            for (int c = 0; c < tables.Count(); c++)
            {
                tmrTimer[c] = new Timer();
                //base on project capacity
                //feeding inbox_center_smses
                //8 insert each 5 sec = > 96/min = 135k/day
                //tmrTimer[c].Interval = 5000;
                tmrTimer[c].Interval = 2000;
                tmrTimer[c].Tag = c;
                tmrTimer[c].Tick += new EventHandler(timer_Tick);

                db[c] = new MariaDB(textBox, tmrTimer[c]);

                if (db[c].status())
                {
                    tmrTimer[c].Enabled = true;
                }

                //Task.Delay(100).Wait();
            }

        }

        private void timer_Tick(object sender, EventArgs e)
        {
            //https://stackoverflow.com/questions/1019793/how-can-i-convert-string-to-int
            int sender_id;
            Int32.TryParse(((Timer)sender).Tag.ToString(), out sender_id);

            switch (sender_id)
            {
                case (0):
                    //int queryLimit = 8;
                    string query_msisdn = @"
SELECT r.*
        FROM(
            SELECT FLOOR(mm.min_id + (mm.max_id - mm.min_id + 1) * RAND()) AS id
                FROM(
                    SELECT MIN(id) AS min_id,
                           MAX(id) AS max_id
                        FROM `#1bot_msisdn_targets`
                     ) AS mm
                JOIN(SELECT id FROM `#1bot_msisdn_targets` where status='active' LIMIT 11 ) z
             ) AS init
        JOIN  `#1bot_msisdn_targets` AS r  ON r.id = init.id
        LIMIT 10;
";
                    //dbQueryResults = db[sender_id].sqlQuery(query + queryLimit.ToString());
                    dbQueryResults = db[sender_id].sqlQuery(query_msisdn);

                    if (dbQueryResults.Count() == 0)
                    {

                    }
                    else
                    {
                        int c = 1;
                        foreach (Dictionary<string, string> record in dbQueryResults)
                        {
                            //sendMessage(record["msisdn"] );

                            // do some processing here....
                            // then updating that record already processed

                            string product = getProduct(record["msisdn"]);

                            //sendMessage(product + "." + record["msisdn"] );

                            string[] customer_separator_pin = getCustomerNodeAndPin().Split('|');
                            sendMessage(customer_separator_pin[0] + " =========>> " + product + "." + record["msisdn"] + "." + customer_separator_pin[1] );
                            //terminal_name, customer_id, customer_node_id ==> find by trigger
                            db[sender_id].Insert("inbox_center_smses", 
                                String.Format("(terminal_id,msisdn,content, sms_datetime) values ('{0}', '{1}', '{2}', '{3}')", 1, customer_separator_pin[0], product + "." + record["msisdn"] + "." + customer_separator_pin[1], DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"))
                                );
                            c++;
                        }

                    }
                    break;

            }



            ////speeding up or down
            //if (dbQueryResults.Count() > queryLimit)
            //{
            //    //tmrTimer[sender_id].Interval = 4000;
            //    tmrTimer[sender_id].Interval = 2000;
            //}
            //else
            //    tmrTimer[sender_id].Interval = 2000;
            ////tmrTimer[sender_id].Interval = 10000;

        }

        private string  getProduct(string msisdn)
        {
            Dictionary<string, string>[] _dbQueryResults;
            MariaDB _db = new MariaDB();

            // 4 from "SELECT MAX(LENGTH(prefix)) FROM prefixes"
            string _prefix_msisdn = msisdn.Substring(0, Math.Min(msisdn.Length, 4)); 

            string query_product = @"
select a.code, b.prefix from 
products as a
join
prefixes as b
where 
a.provider_id = b.provider_id
and
value in (5,10,20,25,30,50,100)
and 
prefix='" + _prefix_msisdn + @"'
order by rand()
limit 1
";
            _dbQueryResults = _db.sqlQuery(query_product);

            if (_dbQueryResults.Count() == 1)
            {
                return _dbQueryResults[0]["code"];
            }
            else
            {
                return "error";
            }
        }

        private string getCustomerNodeAndPin()
        {
            Dictionary<string, string>[] _dbQueryResults;
            MariaDB _db = new MariaDB();

            string query_product = @"
select a.node, b.pin, b.balance from 
customer_nodes as a
join
customers as b
where b.id = a.customer_id 
and 
b.balance > 5000
order by rand()
limit 1
";
            _dbQueryResults = _db.sqlQuery(query_product);

            if (_dbQueryResults.Count() == 1)
            {
                return _dbQueryResults[0]["node"] + "|" + _dbQueryResults[0]["pin"];
            }
            else
            {
                return "error";
            }
        }

        private void sendMessage(string str)
        {
            // auto scroll => https://stackoverflow.com/questions/218732/how-do-i-execute-code-after-a-form-has-loaded   <<= "AfterLoading"

            //Console.WriteLine(textBox.Lines.Length.ToString());
            int lines = 100;
            if (textBox.Lines.Length > lines)//limit to 6 lines here
            {
                //int index = textBox.Text.IndexOf(Environment.NewLine);
                //Console.WriteLine(index.ToString());
                //textBox.Select(0, index + 2);// +2 for \n\r
                //textBox.Cut();
                string[] newLines = new string[lines];
                Array.Copy(textBox.Lines, 1, newLines, 0, lines);
                textBox.Lines = newLines;
            }
            //Console.WriteLine(textBox.Lines.Length.ToString());
            //Console.WriteLine("--------");
            textBox.AppendText(DateTime.Now.ToString() + " -> " + str + Environment.NewLine);
            textBox.SelectionStart = textBox.Text.Length;
            textBox.ScrollToCaret();
        }


    }
}
